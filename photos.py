#!/usr/bin/env python3.5

import helpers
import re
import urllib.request
from lxml import html
from models import Person

photos_folder = r"../www/public/photos/"
source = urllib.request.urlopen("http://w1.c1.rada.gov.ua/pls/site2/fetch_mps?skl_id=8")
content = source.read().decode("Windows-1251")
htmlContent = html.fromstring(content)
deputyImages = {}

for deputy in htmlContent.xpath("//ul[contains(@class, 'search-filter-results')]/li"):
    name = deputy.xpath("./p[@class='title']/a/text()")[0]
    image = deputy.xpath("./p[@class='thumbnail']/img")[0].attrib["src"]
    deputyImages[name] = image

for person in Person.select().where((Person.photo == "")).execute():
    full_name = "%s %s %s" % (person.surname, person.firstname, person.patronymic)
    foundImage = ""
    photo = ""

    if deputyImages.get(full_name, None) is not None:
        foundImage = deputyImages[full_name]
        print("Знайдено %s ?" % deputyImages[full_name])

    photo = input("Шукаємо фото для \033[1m%s\033[0m:" % full_name)

    if foundImage is not "" and photo in ["", "y", "yes", "1"]:
        photo = foundImage

    if photo is not "":
        fileNameMatches = re.search(r"[^/]*$", photo, re.M | re.I)
        targetPath = photos_folder + fileNameMatches.group(0)

        localPath = helpers.download(photo, targetPath)
        person.photo = localPath
        person.save()
        print("Успіх!\n------")
