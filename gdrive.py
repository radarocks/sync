from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from apiclient.http import MediaFileUpload
from apiclient.http import BatchHttpRequest
import oauth2client
from oauth2client import client
from oauth2client import tools
from json import dumps as json_dumps
import logging

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

RW_SCOPE = 'https://www.googleapis.com/auth/drive'
RO_SCOPE = 'https://www.googleapis.com/auth/drive.metadata.readonly'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Drive API Python Quickstart'
CHUNKSIZE = 2 * 1024 * 1024
PARENT_FOLDER_ID = '0B4kg3kTUDWg9bG90XzV2UzVEb0E'
logging.basicConfig(filename='debug.log',level=logging.DEBUG)

def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'drive-python-quickstart.json')

    store = oauth2client.file.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, RW_SCOPE)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def batch_callback(request_id, response, exception):
#   print ("Response for request_id (%s):" % request_id)
#   print (response)

  # Potentially log or re-raise exceptions
  if exception:
    raise exception

def upload(file):
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v2', http=http)

    if os.path.isfile(file):
        filename = os.path.basename(file)
        body = {
            'title': filename,
            'parents': [{
                'id': PARENT_FOLDER_ID
            }]
        }

        media = MediaFileUpload(file, chunksize=CHUNKSIZE, resumable=True)
        if not media.mimetype():
            media = MediaFileUpload(file, DEFAULT_MIMETYPE, resumable=True)
        response = service.files().insert(
            body=body,
            media_body=media).execute()

        if response.get('id'):
            batch_request = BatchHttpRequest(callback=batch_callback)
            batch_entry = service.permissions().insert(fileId=response.get('id'), body={
                'value': '',
                'type': 'anyone',
                'role': 'reader'
            })
            batch_request.add(batch_entry, request_id="batch1")
            batch_request.execute(http)

        return response

    # results = service.files().list(maxResults=10).execute()
    # items = results.get('items', [])
    # if not items:
    #     print('No files found.')
    # else:
    #     print('Files:')
    #     for item in items:
    #         print('{0} ({1})'.format(item['title'].encode('utf-8'), item['id'].encode('utf-8')))
