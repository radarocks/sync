#!/usr/bin/env python3.5

import time
import urllib.request
import urllib.error
import peewee
import gdrive
import googleapiclient
import logger
import helpers
import models as models
from lxml import etree
from db import database


def handle_remote_document(remote):
    local = r"tmp/"

    try:
        # Download from remote server
        for download_attempt in range(1, 3):
            try:
                local_file = helpers.download(remote, local)
            except Exception as e:
                logger.log("Download attempt #%s failed. Reason: %s" % (download_attempt, e))
                logger.log("Retry in 10 seconds...")
                time.sleep(10)
            else:
                print("-"[:1]*100)
                break
        else:
            logger.log("All download attempts failed. Exiting")
            return False

        # Upload to Google Drive
        for upload_attempt in range(1, 3):
            try:
                result = gdrive.upload(local_file)
            except googleapiclient.errors.HttpError as e:
                logger.log("Upload attempt #%s failed. GoogleAPI Error: %s %s" % (upload_attempt, remote, e))
                logger.log("Retry in 10 seconds...")
                time.sleep(10)
            else:
                break
        else:
            logger.log("All upload attempts failed %s" % remote)
            return False

        if result:
            return result.get("id")

    except urllib.error.HTTPError as e:
        logger.log("HTTP Error: %s. %s" % (e.code, remote))
        return False


def get_or_create_person(person_id, first_name, surname, patronymic):
    try:
        if person_id is not None:
            person = models.Person.select().where(models.Person.id == person_id).get()
        elif patronymic is not None:
            try:
                person = models.Person.select().where(
                    (models.Person.firstname == first_name) &
                    (models.Person.surname == surname) &
                    (models.Person.patronymic == patronymic)
                ).get()
            except models.Person.DoesNotExist as e:
                raise e
            except Exception as e:
                logger.log("Failed handle person. Bill #%s. Details: %s" % (person_id, e))
        else:
            person = models.Person.select().where(
                (models.Person.firstname == first_name) &
                (models.Person.surname == surname)
            ).get()
    except models.Person.DoesNotExist:
        person = models.Person.create(
            id=person_id,
            firstname=first_name,
            surname=surname,
            patronymic=patronymic
        )

    return person


def sync(file_name):
    start_time = time.time()
    logger.log("Sync script start")

    database.get_conn().ping(True)

    bills_xml_stream = etree.iterparse(file_name, tag="bill", encoding="utf-8")
    for event, element in bills_xml_stream:
        bill_id = element.attrib["id"]
        bill_type, created = models.BillType.get_or_create(title=element.find("type").text.encode("utf-8"))
        bill_rubric, created = models.BillRubric.get_or_create(title=element.find("rubric").text.encode("utf-8"))
        bill_subject, created = models.BillSubject.get_or_create(title=element.find("subject").text.encode("utf-8"))

        try:
            bill, created = models.Bill.get_or_create(
                id=bill_id,
                bill_type=bill_type,
                rubric=bill_rubric,
                subject=bill_subject
            )
        except peewee.IntegrityError as e:
            logger.log("Failed to add bill %s. Reason: %s" % (bill_id, e))
            continue

        if created:
            logger.log("Bill: %s" % element.attrib["id"])

        bill.title = element.find("title").text.encode("utf-8")
        bill.uri = element.find("uri").text.encode("utf-8")
        bill.number = element.find("number").text.encode("utf-8")
        bill.registered_at = element.find("registrationDate").text.encode("utf-8")

        bill.save()

        for initiator_type in ["outer", "official"]:
            for xml_initiator in element.findall("initiators/initiator/%s/person" % initiator_type):
                initiator_id = xml_initiator.attrib["id"] if "id" in xml_initiator.attrib else None
                first_name = xml_initiator.find("firstname").text.encode("utf-8")
                surname = xml_initiator.find("surname").text.encode("utf-8")
                patronymic = ""

                if xml_initiator.find("patronymic").text is not None:
                    patronymic = xml_initiator.find("patronymic").text.encode("utf-8")

                person = get_or_create_person(initiator_id, first_name, surname, patronymic)

                models.BillInitiator.get_or_create(
                    type=initiator_type,
                    bill=bill,
                    person=person
                )
                del initiator_id
                del person

        for document_type in ["source", "workflow"]:
            for xml_document in element.findall("documents/%s/document" % document_type):
                uri_element = xml_document.find("uri")

                if uri_element is not None:
                    try:
                        models.Document.get(uri=uri_element.text)
                    except models.Document.DoesNotExist:
                        gdoc_id = handle_remote_document(uri_element.text)

                        if gdoc_id is not False:
                            models.Document.create(
                                bill=bill,
                                title=xml_document.find("type").text,
                                date=xml_document.find("date").text if xml_document.find("date") is not None else "",
                                uri=uri_element.text,
                                gdoc_uri="https://drive.google.com/file/d/" + gdoc_id + "/preview",
                                type=document_type
                            )
                            time.sleep(2)
                        else:
                            logger.log("Upload failed. Reason unknown. URL: %s" % uri_element.text)

        element.clear()
    logger.log("Sync script done in %s s." % (time.time() - start_time))
