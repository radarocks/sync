#!/usr/bin/env python3.5

import datetime
import time
import re
import urllib.request
import urllib.error
import os
import logger

xml_url = "http://data.rada.gov.ua/ogd/zpr/skl8/bills.xml"


def download(source_url):
    local = r"./"

    try:
        u = urllib.request.urlopen(source_url)
        h = u.info()
        today = datetime.date.today()
        total_size = int(h["Content-Length"])

        local = local + "bills-" + today.strftime("%d-%m-%Y") + ".xml"
        logger.log("Downloading %s bytes..." % total_size)
        fp = open(local, 'wb')

        block_size = 8 * 1024  # 100000 # urllib.urlretrieve uses 8192
        count = 0
        while True:
            if count > 0 and count % 500 == 0:
                time.sleep(10)

            chunk = u.read(block_size)
            if not chunk: break
            fp.write(chunk)
            count += 1

        logger.log("Downloading has been completed")
        fp.flush()
        fp.close()
        return local
    except urllib.error.HTTPError as e:
        logger.log("HTTP Error: %s. %s" % (e.code, source_url))
    except urllib.error.URLError as e:
        logger.log("urllib.URLError. Retry")
        download(source_url)


def convert(file_path):
    from_fp = open(file_path, "rb")
    to_fp = open(file_path + "x", "wb")
    block_size = 16 * 1024

    while True:
        chunk = from_fp.read(block_size)

        if not chunk:
            break

        if chunk.find(b'version="1.0" encoding="Windows-1251"') > 0:
            chunk = chunk.replace(b'version="1.0" encoding="Windows-1251"', b'version="1.0" encoding="UTF-8"')

        if chunk.find(b' xmlns="http://static.rada.gov.ua/site/bills/opendata/bills.xsd"') > 0:
            chunk = chunk.replace(b' xmlns="http://static.rada.gov.ua/site/bills/opendata/bills.xsd"', b'')

        to_fp.write(chunk.decode("Windows-1251").encode("utf-8"))

    from_fp.flush()
    to_fp.flush()
    from_fp.close()
    to_fp.close()

    os.remove(file_path)
    os.rename(file_path + "x", file_path)
