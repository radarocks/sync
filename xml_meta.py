import logger

xml_type = "xml"
base_url = "http://data.rada.gov.ua"

class XmlMeta:
    def __init__(self, xml_data):
        self.xml_data = xml_data

        logger.log(self.xml_data.findtext("format"))
        extensions = self.xml_data.findtext("format").split(",")

        for index, ext in enumerate(extensions):
            if ext == xml_type:
                self.xml_type_index = index

    def get_url(self):
        path = self.xml_data.findtext("path")
        name = self.xml_data.findtext("name")
        
        return "%s%s%s.%s" % (
            base_url,
            path,
            name,
            xml_type
        )

    def get_size(self):
        sizes = self.xml_data.findtext("size").split(",")
        offset = len(xml_type) + 1

        return sizes[self.xml_type_index][offset:]

    def get_pubdate(self):
        return self.xml_data.findtext("pubDate")

    def get_checksum(self):
        checksums = self.xml_data.findtext("checksum").split(",")
        offset = len(xml_type) + 1

        return checksums[self.xml_type_index][offset:]
