import peewee
import logging
from configparser import ConfigParser

config = ConfigParser()
config.read(r'./db.conf')
config.sections()

database = peewee.MySQLDatabase(config.get("DB", "NAME"),
                                user=config.get("DB", "USER"),
                                host=config.get("DB", "HOST"),
                                passwd=config.get("DB", "PASS"),
                                charset='utf8mb4')

peewee_logger = logging.getLogger('peewee')
peewee_logger.setLevel(logging.ERROR)
