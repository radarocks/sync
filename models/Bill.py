import peewee
from .BaseModel import BaseModel
from .BillType import BillType
from .BillRubric import BillRubric
from .BillSubject import BillSubject


class Bill(BaseModel):
    title = peewee.TextField(null=True)
    uri = peewee.CharField(null=True)
    number = peewee.CharField(null=True)
    registered_at = peewee.DateField(null=True)
    rubric = peewee.ForeignKeyField(BillRubric, related_name='bill')
    subject = peewee.ForeignKeyField(BillSubject, related_name='bill')
    bill_type = peewee.ForeignKeyField(BillType, related_name='bill')

    class Meta:
        db_table = 'bills'
