import peewee
from .BaseModel import BaseModel


class Person(BaseModel):
    surname = peewee.CharField()
    firstname = peewee.CharField()
    patronymic = peewee.CharField()
    photo = peewee.CharField()

    class Meta:
        db_table = 'persons'
