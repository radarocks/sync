import peewee
from datetime import *
from db import database


class BaseModel(peewee.Model):
    created_at = peewee.DateTimeField(default=datetime.now)
    updated_at = peewee.DateTimeField(default=datetime.now)

    def save(self, *args, **kwargs):
        if self._meta.primary_key is not None and self._get_pk_value() is None:
            self.created_at = datetime.now()

        self.updated_at = datetime.now()

        return super(BaseModel, self).save(*args, **kwargs)

    class Meta:
        database = database
