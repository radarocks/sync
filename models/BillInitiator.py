import peewee
from .Bill import Bill
from .Person import Person
from .BaseModel import BaseModel


class BillInitiator(BaseModel):
    type = peewee.CharField()
    bill = peewee.ForeignKeyField(Bill)
    person = peewee.ForeignKeyField(Person)

    class Meta:
        db_table = 'bills_initiators'
