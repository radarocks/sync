import peewee
from .BaseModel import BaseModel


class BillRubric(BaseModel):
    title = peewee.CharField()

    class Meta:
        db_table = 'rubrics'
