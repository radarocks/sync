import peewee
from .BaseModel import BaseModel


class BillType(BaseModel):
    title = peewee.CharField()

    class Meta:
        db_table = 'bill_types'
