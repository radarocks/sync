import peewee
from .Bill import Bill
from .BaseModel import BaseModel
from .EnumField import EnumField


class Document(BaseModel):
    # bill_id = peewee.IntegerField()
    title = peewee.CharField()
    # type = EnumField(choices=["source", "workflow"])
    type = peewee.CharField()
    uri = peewee.CharField()
    date = peewee.DateField()
    gdoc_uri = peewee.CharField()
    bill = peewee.ForeignKeyField(Bill, related_name='documents')

    class Meta:
        db_table = 'documents'
