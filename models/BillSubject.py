import peewee
from .BaseModel import BaseModel


class BillSubject(BaseModel):
    title = peewee.CharField()

    class Meta:
        db_table = 'bill_subjects'
