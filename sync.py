#!/usr/bin/env python

from datetime import *
import models as models
import time
import urllib.error
import gdrive
import xml.etree.ElementTree as ET
import googleapiclient
import logger
import helpers


def handle_remote_document(remote):
    local = r"tmp/"

    try:
        # Download from remote server
        for downloadAttempt in range(1, 3):
            try:
                local_file = helpers.download(remote, local)
            except Exception as e:
                logger.log("Download attempt #%s failed. Reason: %s" % (downloadAttempt, e))
                logger.log("Retry in 10 seconds...")
                time.sleep(10)
            else:
                print("-"[:1]*100)
                break
        else:
            logger.log("All download attempts failed. Exiting")
            return False

        # Upload to Google Drive
        for uploadAttempt in range(1, 3):
            try:
                result = gdrive.upload(local_file)
            except googleapiclient.errors.HttpError as e:
                logger.log("Upload attempt #%s failed. GoogleAPI Error: %s %s" % (uploadAttempt, remote, e))
                logger.log("Retry in 10 seconds...")
                time.sleep(10)
            else:
                break
        else:
            logger.log("All upload attempts failed %s" % remote)
            return False

        if result:
            return result.get('id')

    except urllib.error.HTTPError as e:
        logger.log("HTTP Error: %s. %s" % (e.code, remote))
        return False


def sync(file_name):
    logger.log("Sync script start")
    source = open(file_name)
    bills_xml = ET.parse(source).getroot()
    logger.log("Read file %s done" % file_name)

    for xml_bill in bills_xml:
        row, created = models.Bill.get_or_create(id=xml_bill.attrib['id'])
        if created:
            logger.log("Bill: %s" % xml_bill.attrib['id'])
        bill_type, created = models.BillType.get_or_create(title=xml_bill.find('type').text)
        bill_rubric, created = models.BillRubric.get_or_create(title=xml_bill.find('rubric').text)
        bill_subject, created = models.BillSubject.get_or_create(title=xml_bill.find('subject').text)

        row.title = xml_bill.find('title').text
        row.uri = xml_bill.find('uri').text
        row.number = xml_bill.find('number').text
        row.registered_at = xml_bill.find('registrationDate').text

        if bill_type is not None:
            row.bill_type = bill_type

        if bill_rubric is not None:
            row.rubric_id = bill_rubric.id

        if bill_subject is not None:
            row.subject_id = bill_subject.id

        row.save()

        for xml_initiator in xml_bill.findall('initiators/initiator/official/person'):
            initiator_id = xml_initiator.attrib['id'] if 'id' in xml_initiator.attrib else None
            first_name = xml_initiator.find('firstname').text
            surname = xml_initiator.find('surname').text
            patronymic = xml_initiator.find('patronymic').text

            try:
                if initiator_id is not None:
                    person = models.Person.select().where(models.Person.id == initiator_id).get()
                elif patronymic is not None:
                    try:
                        person = models.Person.select().where(
                            models.Person.firstname == first_name &
                            models.Person.surname == surname &
                            models.Person.patronymic == patronymic.encode('utf-8')
                        ).get()
                    except Exception as e:
                        logger.log("Failed handle person. Bill #%s. Details: %s" % (row.id, e))
                else:
                    person = models.Person.select().where(
                        models.Person.firstname == first_name &
                        models.Person.surname == surname
                    ).get()
            except models.Person.DoesNotExist:
                person = models.Person.create(
                    id=initiator_id,
                    firstname=first_name,
                    surname=surname,
                    patronymic=patronymic
                )

            models.BillInitiator.get_or_create(
                type='official',
                bill=row,
                person=person
            )

        for xml_document in xml_bill.findall('documents/source/document'):
            uri_element = xml_document.find('uri')

            if uri_element is not None:
                try:
                    models.Document.get(uri=uri_element.text)
                except models.Document.DoesNotExist:
                    gdoc_id = handle_remote_document(uri_element.text)

                    if gdoc_id is not False:
                        models.Document.create(
                            bill=row,
                            title=xml_document.find('type').text,
                            date=xml_document.find('date').text if xml_document.find('date') is not None else '',
                            uri=uri_element.text,
                            gdoc_uri='https://drive.google.com/file/d/' + gdoc_id + '/preview',
                            type='source'
                        )
                        time.sleep(2)
                    else:
                        logger.log("Upload failed. Reason unknown. URL: %s" % uri_element.text)

        for xml_document in xml_bill.findall('documents/workflow/document'):
            uri_element = xml_document.find('uri')

            if uri_element is not None:
                try:
                    models.Document.get(uri=uri_element.text)
                except models.Document.DoesNotExist:
                    gdoc_id = handle_remote_document(uri_element.text)

                    if gdoc_id is not False:
                        models.Document.create(
                            bill=row,
                            title=xml_document.find('type').text,
                            date=xml_document.find('date').text if xml_document.find('date') is not None else '',
                            uri=uri_element.text,
                            gdoc_uri='https://drive.google.com/file/d/' + gdoc_id + '/preview',
                            type='workflow'
                        )
                        time.sleep(2)
                    else:
                        logger.log("Upload failed. Reason unknown. URL: %s" % uri_element.text)
    logger.log("Sync script done")
    source.close()
    bills_xml.clear()
