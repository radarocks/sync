#!/usr/bin/env python3.5

import datetime
import time
import urllib.request
import urllib.error
import hashlib
import xml.etree.ElementTree as ET
import gc
import os
import helpers
import sync_v2 as sync_lib
import logger
import zipfile
from xml_meta import XmlMeta
from configparser import ConfigParser

CHECK_INTERVAL = 60 * 60
config = ConfigParser()
config.read(r'./sync.conf')
config.sections()
if config.has_section('sync') is False:
    config.add_section('sync')


def save_config(config):
    with open(r'./sync.conf', 'w') as configfile:
        config.write(configfile)


def try_sync():
    file_path = config.get('sync', 'last_file')
    for sync_attempt in range(1, 3):
        try:
            sync_lib.sync(file_path)
            config.set('sync', 'succeed', 'True')
            save_config(config)
        except Exception as e:
            logger.log("Sync failed. Retry in 5 minutes. Reason: %s" % e)
            time.sleep(300)
        else:
            break


def unzip_xml(file_path):
    today = datetime.date.today()
    save_path = r"./bills-%s.xml" % today.strftime("%d-%m-%Y")

    with zipfile.ZipFile(file_path, "r") as z:
        logger.log("unzipping bills-skl8.xml")
        z.extract("bills-skl8.xml", r"./")
        os.rename(r"./bills-skl8.xml", save_path)

        return save_path


def check():
    url = "http://data.rada.gov.ua/ogd/zpr/skl8/meta.xml"

    for attempt in range(1, 3):
        try:
            source = urllib.request.urlopen(url)
            data = source.read()
            source.close()
        except Exception as e:
            logger.log("Failed to open meta file. Attempt #%s" % attempt)
            logger.log(e)
            time.sleep(5)
        else:
            break

    data_item_xml = ET.fromstring(data).find('meta/item[@type="data"]')
    xml_meta = XmlMeta(data_item_xml)
    pub_date = xml_meta.get_pubdate()

    if config.has_option('sync', 'last_date') is False or config.get('sync', 'last_date') != pub_date:
        today = datetime.date.today()
        xml_url = xml_meta.get_url()
        save_path = r"./bills-%s.xml" % today.strftime("%d-%m-%Y")

        for attempt in range(1, 7):
            try:
                file_path = helpers.download(xml_url, save_path)
                # xml_file = unzip_xml(file_path)
                hashsum = hashlib.md5(open(file_path, 'rb').read()).hexdigest()
                checksum = xml_meta.get_checksum()
                logger.log(checksum)

                if hashsum != checksum:
                    raise Exception("Checksum is not correct")

            except Exception as e:
                logger.log("Failed to download attempt #%s" % attempt)
                logger.log(e)
                time.sleep(10)
            else:
                break
        else:
            logger.log("All attempts failed. Exit")
            return

        logger.log("Convert from Windows-1251 to UTF-8: %s" % file_path)
        helpers.convert(file_path)
        config.set('sync', 'succeed', 'False')
        config.set('sync', 'last_date', pub_date)
        config.set('sync', 'last_file', file_path)
        save_config(config)

        try_sync()
    elif config.get('sync', 'last_date') == pub_date and (
        config.has_option('sync', 'succeed') is False or config.get('sync', 'succeed') == 'False'
    ):
        try_sync()
    else:
        logger.log("Last sync date: %s; Current sync file date: %s" % (config.get('sync', 'last_date'), pub_date))
    # free memory
    data_item_xml.clear()

while 1:
    logger.log("Start check script")
    check()
    logger.log("Next check in: %s seconds" % CHECK_INTERVAL)
    gc.collect()
    time.sleep(CHECK_INTERVAL)
