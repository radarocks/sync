import time
import re
import urllib.request
import urllib.error
import os
import io
import sys
import logger
import socket
from threading import Timer


def download(url, target):
    try:
        logger.log("Download: %s" % url)
        source = urllib.request.urlopen(url, timeout=300)
        source_info = source.info()

        try:
            total_size = int(source_info["Content-Length"])
        except KeyError:
            logger.log("Download failed. File has no `Content-Length`")
            return False

        if os.path.isdir(target) is True:
            match_obj = re.search(r'filename="([^\"]+)\"', source_info["Content-Disposition"], re.M | re.I)
            target_file = target + match_obj.group(1)
        else:
            target_file = target

        if target_file:
            sys.stdout.write("Download %s bytes.." % total_size)
            sys.stdout.flush()
            fp = open(target_file, 'wb')

            block_size = 8 * 1024  # 100000 # urllib.urlretrieve uses 8192
            count = 0
            while True:
                if count > 0 and count % 500 == 0:
                    sys.stdout.write(".")
                    sys.stdout.flush()
                    time.sleep(10)

                timer = Timer(5.0, fp.close)
                try:
                    timer.start()
                    chunk = source.read(block_size)
                    timer.cancel()
                except socket.error as se:
                    logger.log("Download failed. Socket connection is to slow")
                    print(se)
                    source.close()
                    fp.flush()
                    fp.close()
                    raise

                if not chunk:
                    sys.stdout.write(".")
                    break

                fp.write(chunk)
                count += 1
            sys.stdout.write("\n")

            logger.log("Download has been completed")
            source.close()
            fp.flush()
            fp.close()
            return target_file

    except urllib.error.HTTPError as e:
        logger.log("HTTP Error: %s. %s" % (e.code, url))
    except urllib.error.URLError as e:
        logger.log("urllib.URLError: %s. Retry" % e.reason)
        download(url, target)


def convert(file_path):
    from_fp = io.open(file_path, "r", encoding="Windows-1251")
    to_fp = io.open(file_path + "x", "w", encoding="utf8")
    block_size = 16 * 1024

    while True:
        chunk = from_fp.read(block_size)

        if not chunk:
            break

        if chunk.find('version="1.0" encoding="Windows-1251"') > 0:
            chunk = chunk.replace('version="1.0" encoding="Windows-1251"', 'version="1.0" encoding="UTF-8"')

        if chunk.find(' xmlns="http://static.rada.gov.ua/site/bills/opendata/bills.xsd"') > 0:
            chunk = chunk.replace(' xmlns="http://static.rada.gov.ua/site/bills/opendata/bills.xsd"','')

        to_fp.write(chunk)

    from_fp.flush()
    to_fp.flush()
    from_fp.close()
    to_fp.close()

    os.remove(file_path)
    os.rename(file_path + "x", file_path)
