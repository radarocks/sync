import time
import sys


def log(message):
    print("[%s] %s" % (time.strftime("%d-%m-%Y %H:%M:%S"), message))
    sys.stdout.flush()
